## Copyright 2014-2016,2021 IPB, Universite de Bordeaux, INRIA & CNRS
##
## This file is part of the Scotch software package for static mapping,
## graph partitioning and sparse matrix ordering.
##
## This software is governed by the CeCILL-C license under French law
## and abiding by the rules of distribution of free software. You can
## use, modify and/or redistribute the software under the terms of the
## CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
## URL: "http://www.cecill.info".
##
## As a counterpart to the access to the source code and rights to copy,
## modify and redistribute granted by the license, users are provided
## only with a limited warranty and the software's author, the holder of
## the economic rights, and the successive licensors have only limited
## liability.
##
## In this respect, the user's attention is drawn to the risks associated
## with loading, using, modifying and/or developing or reproducing the
## software by the user in light of its specific status of free software,
## that may mean that it is complicated to manipulate, and that also
## therefore means that it is reserved for developers and experienced
## professionals having in-depth computer knowledge. Users are therefore
## encouraged to load and test the software's suitability as regards
## their requirements in conditions enabling the security of their
## systems and/or data to be ensured and, more generally, to use and
## operate it in the same conditions as regards security.
##
## The fact that you are presently reading this means that you have had
## knowledge of the CeCILL-C license and that you accept its terms.
##
############################################################
##                                                        ##
##   AUTHORS    : Cedric LACHAT                           ##
##                Amaury JACQUES                          ##
##                Florent PRUVOST                         ##
##                Marc FUENTES                            ##
##                                                        ##
##   FUNCTION   : Secondary configuration file for CMake  ##
##                                                        ##
##   DATES      : # Version 6.0  : from : 01 sep 2014     ##
##                                 to     01 sep 2021     ##
##                # Version 7.0  : from : 01 sep 2021     ##
##                                 to     30 dec 2021     ##
##                                                        ##
############################################################

set(SCOTCH_VERSION @SCOTCH_VERSION_LONG@)

# relocatable package
@PACKAGE_INIT@

set_and_check(SCOTCH_INC_DIR "@PACKAGE_INC_INSTALL_DIR@")
set_and_check(SCOTCH_LIB_DIR "@PACKAGE_LIB_INSTALL_DIR@")

check_required_components(SCOTCH)

# dependencies of SCOTCH
include(CMakeFindDependencyMacro)
find_dependency(Threads)
if(@PTSCOTCH@)
  find_dependency(MPI REQUIRED)
endif()

if(@ESMUMPS@)
#
endif()

# add the targets file
include("${CMAKE_CURRENT_LIST_DIR}/scotcherrexitTargets.cmake")
include("${CMAKE_CURRENT_LIST_DIR}/scotcherrTargets.cmake")
include("${CMAKE_CURRENT_LIST_DIR}/scotchTargets.cmake")
if(@PTSCOTCH@)
  include("${CMAKE_CURRENT_LIST_DIR}/ptscotcherrexitTargets.cmake")
  include("${CMAKE_CURRENT_LIST_DIR}/ptscotcherrTargets.cmake")
  include("${CMAKE_CURRENT_LIST_DIR}/ptscotchTargets.cmake")
endif()
if(@ESMUMPS@)
  include("${CMAKE_CURRENT_LIST_DIR}/esmumpsTargets.cmake")
endif()
if(@SCOTCHMETIS@)
  include("${CMAKE_CURRENT_LIST_DIR}/scotchmetisTargets.cmake")
endif()
